FROM docker.io/liyehaha/py3-venv:latest

ENV LANG en_US.UTF-8

ADD ./supervisord.conf /etc/supervisord.conf

RUN mkdir -p /home/works/program/

ADD . /home/works/program/
RUN /home/works/python3/bin/pip3.6 install -r /home/works/program/requirements.txt

RUN mkdir -p /home/works/supervisor/logs

EXPOSE 8888

RUN chown works.works -R /home/works/
RUN yum clean all

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
